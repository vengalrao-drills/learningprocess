#  Prevention of Sexual Harassment

### What behaviors are considered as sexual harassment?

Sexual harassment can happen through words, like making inappropriate comments, visually, such as displaying offensive content, or physically, like unwelcome touching.

### If you witness or experience such behavior, what should you do?

First, tell the person involved that their actions are making you uncomfortable. If it continues, document the incidents and report them to the appropriate authorities.

### What are some scenarios portrayed by actors?

Actors depict situations like expressing creativity responsibly, treating everyone fairly, following agreed-upon terms, avoiding offensive jokes, showcasing positive role models, and being careful with language.

### How to address harassment?

Educate people about what constitutes harassment, encourage reporting, and foster a culture that values respect.

### How to behave appropriately?

Understand that what you find acceptable might not be comfortable for others. Respect personal boundaries and maintain appropriate behavior in different situations.
