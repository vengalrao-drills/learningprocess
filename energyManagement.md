# Manage Energy not Time
## 1: What are the activities you do that make you relax - Calm quadrant?
- Taking a leisurely walk in nature.
- Meditating for a few minutes.
- Engaging in deep-breathing exercises.
- Reading a book.

## 2: When do you find getting into the Stress quadrant?
- Experiencing unexpected challenges at work.
- Juggling multiple tasks with tight deadlines.
- Dealing with interpersonal conflicts.
- Trying to learn a complex skill under time pressure.

## 3: How do you understand if you are in the Excitement quadrant?
- Receiving positive feedback on a project.
- Successfully overcoming a challenging obstacle.
- Reaching a personal milestone or achievement.
- Experiencing a surge of motivation and enthusiasm.

# Sleep is your superpower
## 4: Paraphrase the Sleep is your Superpower video in detail?
- Recognizing the significance of quality sleep for overall well-being.
- Emphasizing the recommended sleep duration of 7-8 hours.
- Highlighting the negative impact of only getting 4 hours of sleep, including a 70% decrease in natural killer cells.
- Offering practical tips for better sleep, such as maintaining a consistent bedtime, creating a cool sleeping environment, and avoiding smartphone use before bedtime.
- Exploring the profound effects of sleep on learning, memory, and the immune system.

## 5: What are some ideas that you can implement to sleep better?
- Establishing a consistent bedtime and wake-up time routine.
- Avoiding the use of smartphones or electronic devices before bedtime.
- Incorporating regular exercise into your lifestyle.
- Opting for lighter meals in the evening to avoid discomfort.

# Brain Changing Benefits of Exercise
## 6: Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points?
- Wendy Suzuki underscores the enduring positive effects of physical activity on overall well-being.
- Explaining the structure of the brain, focusing on the prefrontal cortex responsible for decision-making.
- Sharing personal experiences of sedentary living leading to unhappiness, dissatisfaction, and weight gain.
- Discussing how regular exercise enhances memory and concentration at work.
- Highlighting the role of the hippocampus in storing permanent memories and the extended focus improvement after workouts.
- Emphasizing the need for extended workout durations for lasting cognitive benefits.

## 7: What are some steps you can take to exercise more?
- Opting for stairs instead of elevators whenever possible.
- Incorporating daily exercise routines or gym sessions into your schedule.
- Keeping a log of physical activities like bench press, deadlifts, and squats.
- Ensuring a nutritious diet to support your physical activity levels.
