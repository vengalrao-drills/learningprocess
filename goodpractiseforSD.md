# 1. Which point(s) were new to you?
* Keeping frequent updates and staying closely connected with necessary team members.
* Ensuring proper communication when required.
* Approaching meetings in an effective manner.
* Taking notes on key areas.

# 2. Which area do you think you need to improve on? What are your ideas to make progress in that area?
Communication is crucial for information transfer. I recognize the need to enhance communication, especially when interacting with higher-ranking employees. To make progress, I plan to adopt a balanced approach, being casual when appropriate and maintaining a professional tone. Establishing communication rules can help strike the right balance.
