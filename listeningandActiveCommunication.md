# Active Listening Strategies

### Take Note:
- Jot down the important stuff.

### Stay Focused:
- Tune out distractions, give your full attention.

### Reword It:
- Paraphrase to make sure you're both on the same page.

### Open the Door:
- Use friendly phrases to keep the conversation flowing.

### Zip It:
- Let the speaker finish; no interruptions.

### Body Talk:
- Show you're tuned in with positive body language.

# Fisher's Model of Reflective Listening

### Listen Up:
- Pay attention and reflect on what's being said.

### Feel the Feels:
- Show empathy to keep the conversation engaging.

### Repeat for Clarity:
- Regularly rephrase to make sure everyone's on the same page.

### Open Mind, Share Thoughts:
- Stay open-minded and share your thoughts for a well-rounded conversation.

# Listening Roadblocks

### Distractions:
- Keep your focus.

### Judgment Call:
- Drop preconceived notions.

### Pause the Response:
- Hold off on forming your response while the speaker talks.

# Boosting Listening Skills

### Zen Mode:
- Practice mindfulness for sharper listening.

### Cut the Clutter:
- Minimize distractions to really tune in.

### Eyes on You:
- Make a conscious effort to focus on the speaker.

### Fisher's Wisdom:
- Use Fisher's model to level up your listening game.

# Communication Styles

### Passive Vibes

#### Lost Focus:
- Passive mode due to lack of concentration.

#### Fear Factor:
- Afraid of losing the argument or conflict.

### Aggressive Moves

#### Conversation Dominator:
- Switch to aggressive when you want the spotlight.

#### Volume Boost:
- Raise your voice in anger or when feeling threatened.

### Passive-Aggressive Swirl

#### Narrow-Minded:
- Use passive-aggressive styles when not open to diverse thoughts.

#### Accountability Escape:
- Happens when avoiding responsibility and admitting mistakes.

# Assertive Communication

### Feel & Express:
- Recognize and express your feelings.

### Need Declaration:
- Clearly state what you need.

### Baby Steps:
- Start with less intense feelings.

### Body Check:
- Be aware of your body language.

### Context Check:
- Stay mindful of the situation.
