# How does a browser render HTML, CSS, JS to DOM? What is the mechanism behind it?

The process of how a browser renders HTML, CSS, and JavaScript to create a webpage involves several steps. despite different browser engines (like Blink for Chrome, WebKit for Safari, Gecko for Firefox) and their unique ways of rendering web pages, the fundamental goal remains same that is to present the accurate content, structure, and design of a webpage to the user.
<!-- <br/> -->
HTML structures the content of a webpage using tags like headings, paragraphs, and images. CSS styles the HTML elements, controlling their appearance with properties like colors, fonts, and layout. JavaScript adds interactivity, allowing manipulation of HTML/CSS, responding to user actions, and enabling dynamic changes on the webpage.
## Browser Rendering: A Peek Behind the Scenes
## From Bytes to Understanding

 When you open a webpage, the browser doesn't immediately comprehend your code. It starts by fetching raw data from your files, converting these bytes into characters based on the file's encoding. These characters are then broken down into tokens, akind to small building blocks representing HTML tags.


## Tokenization and Nodes

Tokens are more than just characters; they're structured data about specific HTML elements. These tokens are further converted into nodes, distinct entities forming a Document Object Model (DOM) tree. The DOM connects elements, establishing relationships and structures like parent-child links.
## Styling and CSSOM

While the DOM forms, the browser simultaneously deals with CSS files. Similar to HTML, CSS is processed from raw bytes into a structure known as the CSS Object Model (CSSOM). This structure holds style information and handles how elements are visually presented.
## Merging for Visualization: The Render Tree

Individually, the DOM and CSSOM handle content and style separately. However, to paint the webpage, the browser combines both structures into a render tree. This tree holds information on visible content and styling, excluding elements hidden by CSS rules.
## Calculating Layout

Before showing anything on the screen, the browser calculates the precise size and position of each element through a layout phase. It's like a meticulous planning stage, using content and style information to determine where everything should appear on the viewport.
## Rendering: Bringing Life to the Screen

Finally, with exact element positions defined, the browser 'paints' these elements onto the screen. Utilizing information from the DOM and CSSOM, it displays the webpage, making your design and content visible to the user.
## Challenges with Scripts and Performance

Scripts in your HTML can be both powerful and problematic. When encountering a script tag, the browser pauses DOM construction until the script finishes executing. The script's position matters; placing it before crucial elements can cause issues, interrupting the DOM building process.
## Async Attribute: Resolving Script Delays

To mitigate script-related delays, the 'async' attribute can be used in the script tag. Adding 'async' ensures the DOM construction won't halt, allowing the browser to continue building the page while the script downloads. Once ready, the script executes without hindering the page's construction.
## Syncing JavaScript with CSSOM

JavaScript can also access and modify CSSOM. If a script encounters CSS-related changes, it waits for the CSSOM to be ready before executing. This synchronization ensures JavaScript operations dependent on styles don't execute prematurely.

In essence, from bytes to visual delight, the browser undergoes a complex process to translate your code into a tangible, interactive web experience. Each step, from parsing to painting, contributes to the seamless rendering of the webpage.


# References
- [How does a browser work ? | Engineering side - by Hitesh Chowday](https://youtu.be/5rLFYtXHo9s?si=PD20ITLqLQTuTv5_)

- [How browser rendering works — behind the scenes - blog.logrocket.com](https://blog.logrocket.com/how-browser-rendering-works-behind-scenes/)

